import sys
import numpy as np
import cvxopt
from cvxopt import matrix, spmatrix, solvers

T=int(sys.argv[1])
exp=sys.argv[2]
bM=int(sys.argv[3])
B=int(sys.argv[4])
num_cov=1

ROOT="./datasets/IBM_HR_dataset/GraphStructure/"

f_LHS=open(ROOT + "LHS_types_sim", "r")
LHS=dict()
U=0
av_LHS=dict()
for line in f_LHS:
  vals=line.split("\n")[0]
  LHS[vals]=U
  av_LHS[vals]=True
  U+=1
f_LHS.close()  

print "U", U
'''
f_RHS=open(ROOT + "RHS_types_sim", "r")
RHS=dict()
V=0

for line in f_RHS:
  vals=line.split("\n")[0]
  RHS[vals]=V
  V+=1

print "V", V
f_RHS.close()
'''
f_rates=open(ROOT + "arrival_rates", "r")
RHS=dict()
V=0
for line in f_rates:
  vals=line.split("\n")[0].split(",")
  if vals[0] not in RHS:
    RHS[vals[0]]=dict()
  RHS[vals[0]]["id"]=V
  RHS[vals[0]]["rate"]=float(vals[1])

  V+=1
f_rates.close()
print "V", V

f_edges=open(ROOT + "edges_sim", "r")

edges=dict()
neigh_RHS=dict()
neigh_LHS=dict()
reverse_edge=dict()
E=0

all_features=set()
for line in f_edges:
  vals=line.split("\n")[0].split(",")
  E+=1
  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["weight"]=float(vals[3])
  edges[vals[0]]["features"]=set()
  
  reverse_edge[vals[1] + "<>" + vals[2]]=vals[0]
  if vals[1] not in neigh_RHS:
    neigh_RHS[vals[1]]=set()
  
  if vals[2] not in neigh_LHS:
    neigh_LHS[vals[2]]=set()

  neigh_RHS[vals[1]].add(vals[2])
  neigh_LHS[vals[2]].add(vals[1])

  feat1=set(vals[1].split(";"))
  feat2=set(vals[2].split(";"))
  feat=feat1.union(feat2)
  
  for _f in feat:
    edges[vals[0]]["features"].add(_f)
    all_features.add(_f)

f_edges.close()


print "E=", E
# A and b matrix
K=1
resource_map=dict()

for _all in all_features:
  resource_map[_all]=K
  K+=1

K-=1

#Weights
W=[0 for i in xrange(E)]
W=W+[B]

b=list()
b.append(0)
for _v in RHS:
  b.append(RHS[_v]["rate"])
for _u in LHS:
  b.append(bM)

valsl=list()
I=list()
J=list()

f_X=open("LP/Xbudget" + str(bM), "r")
X=dict()
EE=0
for line in f_X:
  vals=line.split("\n")[0]
  X[str(EE)]=float(vals)
  EE+=1

  if EE>=E:
    break
f_X.close()
#+1 in indices to account for matlab 1 index
for _e in xrange(E):
  weight=edges[str(_e)]["weight"]
  valsl.append(-weight)
  I.append(1)
  J.append(_e + 1)
  
valsl.append(B)
I.append(1)
J.append(E + 1)

for _v in RHS:
  tsum=0
  print "V=", _v
  for _neigh_RHS in neigh_RHS[_v]:
    _edge=_v + "<>" + _neigh_RHS
    _edgeNum=reverse_edge[_edge]
    tsum+=X[_edgeNum]
  print _edgeNum, tsum, RHS[_v]["rate"], _v, RHS[_v]["id"]
  assert(tsum<=RHS[_v]["rate"]+0.01)
for _u in LHS:
  tsum=0
  for _neigh_LHS in neigh_LHS[_u]:
    _edge=_neigh_LHS + "<>" + _u
    _edgeNum=reverse_edge[_edge]
    tsum+=X[_edgeNum]
    
  assert(tsum<=1.01)

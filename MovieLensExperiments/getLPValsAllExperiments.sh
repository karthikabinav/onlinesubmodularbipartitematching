#Bash file to initiate the run of various experiments

#Obtaining the LP values using the python script to generate the sparse matrices

#Experiment1-Plot 1
#eta=1
declare -a arr=(1 2 3 5 10 15)
for i in "${arr[@]}"
do
  python getLP_coverage.py 1000 $i 1
done

: <<'COMMENT'
#Experiment1-Plot 2
#eta=3
for i in "${arr[@]}"
do
  python getLP_coverage.py 1000 $i 3
done

#Experiment1-Plot 3
#eta=5
for i in "${arr[@]}"
do
  python getLP_coverage.py 1000 $i 5
done
COMMENT

#Solving the LP using MATLAB
module load matlab/2016b

#Experiment1-Plot 1
#eta=1
declare -a arr=(1 2 3 5 10 15)
for i in "${arr[@]}"
do
  matlab -nodisplay -nosplash -nodesktop -r "expe='$i:1';run('~/submodularMatching/MovieLensExperiments/lp_solver.m');exit;"""
done

: <<'COMMENT'
#Experiment1-Plot 2
#eta=3
for i in "${arr[@]}"
do
  matlab -nodisplay -nosplash -nodesktop -r "expe='$i:3';run('~/submodularMatching/MovieLensExperiments/lp_solver.m');exit;"""
done

#Experiment1-Plot 3
#eta=5
for i in "${arr[@]}"
do
  matlab -nodisplay -nosplash -nodesktop -r "expe='$i:5';run('~/submodularMatching/MovieLensExperiments/lp_solver.m');exit;"""
done
COMMENT

import sys
import numpy as np
import cvxopt
from cvxopt import matrix, spmatrix, solvers
from pprint import pprint


T=int(sys.argv[1])
bM=int(sys.argv[2])
eta=int(sys.argv[3])
exp=str(bM) + ":" + str(eta)
num_cov=1

ROOT="./ml-1m/processed_dataset/"

f_LHS=open(ROOT + "reduced_movies.dat", "r")
LHS=dict()
av_LHS=dict()
LHS_pos=dict()
U=0
for line in f_LHS:
  vals=line.split("\n")[0]
  LHS[vals]=int(vals.split("::")[0])
  av_LHS[vals]=True
  LHS_pos[int(vals.split("::")[0])]=U
  U+=1
f_LHS.close()  

f_RHS=open(ROOT + "reduced_users_withArrivals.dat", "r")
RHS=dict()
RHS_pos=dict()
rates=dict()
V=0
for line in f_RHS:
  vals=line.split("\n")[0]
  RHS[vals]=int(vals.split("::")[0])
  rates[int(vals.split("::")[0])]=float(vals.split("::")[5])
  RHS_pos[int(vals.split("::")[0])]=V
  V+=1
f_RHS.close()
#print pprint(RHS_pos)
f_edges=open(ROOT + "GraphStructure/edges.dat", "r")

edges=dict()
neigh_RHS=dict()
neigh_LHS=dict()
reverse_edge=dict()
E=0

all_features=set()
for line in f_edges:
  vals=line.split("\n")[0].split(",")
  E+=1
  
  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["features"]=set()
  
  reverse_edge[vals[1] + "<>" + vals[2]]=vals[0]
  if vals[1] not in neigh_RHS:
    neigh_RHS[vals[1]]=set()
  
  if vals[2] not in neigh_LHS:
    neigh_LHS[vals[2]]=set()

  neigh_RHS[vals[1]].add(vals[2])
  neigh_LHS[vals[2]].add(vals[1])

  feat=set(vals[3].split("|"))
  
  for _f in feat:
    edges[vals[0]]["features"].add(_f)
    all_features.add(_f)

f_edges.close()

print E
# A and b matrix
K=0
resource_map=dict()

for _all in all_features:
  resource_map[_all]=K
  K+=1

fWeights=dict()
f_fweights=open(ROOT + "GraphStructure/feature_weights.dat", "r")
#feature, RHS, weight
for line in f_fweights:
  vals=line.split("\n")[0].split(",")
  if vals[0] not in fWeights:
    fWeights[vals[0]]=dict()
  fWeights[vals[0]][vals[1]]=float(vals[2])

#Weights
W=[0 for i in xrange(E + K*V)]

for _feat in all_features:
  for _v in RHS:
    _k=resource_map[_feat]
    v_pos=RHS_pos[RHS[_v]]
    W[E+_k*V + v_pos]=fWeights[_feat][str(RHS[_v])]

b=[0 for i in xrange(K*V+U+V)]
valsl=list()
I=list()
J=list()

#+1 in indices to account for matlab 1 index
for _e in xrange(E):
  for _feat in edges[str(_e)]["features"]:
    #for v_pos in xrange(V):
    _v = int(edges[str(_e)]["RHS"])
    v_pos=RHS_pos[_v]
    valsl.append(-1)
    _k=resource_map[_feat]
    I.append(_k*V + v_pos + 1)
    J.append(_e + 1)

print "Done Block 1"
for _k in xrange(K):
  for _v in xrange(V):
    valsl.append(1)
    I.append(_k*V + _v + 1)
    J.append(E + _k*V + _v + 1)

print "Done Block 2"
print V*K
for _v in RHS_pos:
  b[RHS_pos[_v]+K*V]=T*rates[_v]*eta
  #b[RHS_pos[_v]+K*V]=1
  for _neigh_RHS in neigh_RHS[str(_v)]:
    _edge=str(_v) + "<>" + _neigh_RHS
    _edgeNum=int(reverse_edge[_edge])
    
    valsl.append(1)
    I.append(K*V + RHS_pos[_v] + 1)
    J.append(_edgeNum + 1)

print "Done Block 3"

for _u in LHS_pos:
  b[K*V+V+LHS_pos[_u]]=bM
  for _neigh_LHS in neigh_LHS[str(_u)]:
    _edge=_neigh_LHS + "<>" + str(_u)
    _edgeNum=int(reverse_edge[_edge])
    
    valsl.append(1)
    I.append(K*V + V + LHS_pos[_u] + 1)
    J.append(_edgeNum + 1)

print "Done Block 4"


f = open("./LP/vals"+exp, "w+")
np.array(valsl).tofile(f, sep=",")
f.close()
f = open("./LP/I"+exp, "w+")
np.array(I).tofile(f, sep=",")
f.close()
f = open("./LP/J"+exp, "w+")
np.array(J).tofile(f, sep=",")
f.close()
f = open("./LP/b"+exp, "w+")
print "b=", len(b)
b=matrix(b)
np.array(b).tofile(f, sep=",")
f.close()
f = open("./LP/c"+exp, "w+")
print "W=", len(W)
W=matrix(W)
np.array(W).tofile(f, sep=",")
f.close()


import sys
import operator

user=sys.argv[1]
k=int(sys.argv[2])

f=open("../processed_dataset/predictions/complete_ratings_" + user, "r")

movies=dict()
first=True
for line in f:
  if first:
    first=False
    continue
  
  vals=line.split("\n")[0].split(";")
  _pred=float(vals[2])
  movies[line]=_pred


sorted_x = reversed(sorted(movies.items(), key=operator.itemgetter(1)))

cnt=0
for key, value in sorted_x:
  if cnt>=k:
    break
  print key, value
  cnt+=1

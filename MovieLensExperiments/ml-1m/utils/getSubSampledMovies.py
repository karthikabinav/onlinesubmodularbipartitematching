import numpy as np

movies_lines=np.random.choice(3883, 100, replace=False)

_id=0
f=open("../original_dataset/movies.dat", "r")
g=open("../processed_dataset/subsampled_movies.dat", "w+")

for line in f:
  if _id in movies_lines:
    g.write(line)
  _id+=1
f.close()
g.close()


import sys
from statistics import median, mean

#Computing the coverage function
def computeF(S):
  coveredE=dict()
  covered=dict()
  for e in S:
    if edges[e]["RHS"] not in coveredE:
      coveredE[edges[e]["RHS"]]=set()
    coveredE[edges[e]["RHS"]].add(e)
  
  for _v in coveredE:
    for e in coveredE[_v]:
      ed_features=edges[e]["features"]
      
      covered[_v]=set()
      for _features in ed_features:
        covered[_v].add(_features)
  total=0
  for _v in covered:
    for _cov in covered[_v]:
      total = total + fWeights[_cov][_v]
  return total

T=int(sys.argv[1])
OPT=float(sys.argv[2])
b=int(sys.argv[3])
eta=int(sys.argv[4])
exp=str(b) + ":" + str(eta)
num_cov=1

ROOT="../ml-1m/processed_dataset/"

f_LHS=open(ROOT + "reduced_movies.dat", "r")
LHS=dict()
av_LHS=dict()
LHS_pos=dict()
U=0
for line in f_LHS:
  vals=line.split("\n")[0]
  LHS[vals]=int(vals.split("::")[0])
  av_LHS[int(vals.split("::")[0])]=True
  LHS_pos[int(vals.split("::")[0])]=U
  U+=1
f_LHS.close()  

f_RHS=open(ROOT + "reduced_users_withArrivals.dat", "r")
RHS=dict()
RHS_pos=dict()
rates=dict()
V=0
for line in f_RHS:
  vals=line.split("\n")[0]
  RHS[vals]=int(vals.split("::")[0])
  rates[int(vals.split("::")[0])]=float(vals.split("::")[5])
  RHS_pos[int(vals.split("::")[0])]=V
  V+=1
f_RHS.close()
f_edges=open(ROOT + "GraphStructure/edges.dat", "r")

edges=dict()
neigh_RHS=dict()
neigh_LHS=dict()
reverse_edge=dict()
E=0

all_features=set()
for line in f_edges:
  vals=line.split("\n")[0].split(",")
  E+=1
  
  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["features"]=set()
  
  reverse_edge[vals[1] + "<>" + vals[2]]=vals[0]
  if vals[1] not in neigh_RHS:
    neigh_RHS[vals[1]]=set()
  
  if vals[2] not in neigh_LHS:
    neigh_LHS[vals[2]]=set()

  neigh_RHS[vals[1]].add(vals[2])
  neigh_LHS[vals[2]].add(vals[1])

  feat=set(vals[3].split("|"))
  
  for _f in feat:
    edges[vals[0]]["features"].add(_f)
    all_features.add(_f)

f_edges.close()

fWeights=dict()
f_fweights=open(ROOT + "GraphStructure/feature_weights.dat", "r")
#feature, RHS, weight
for line in f_fweights:
  vals=line.split("\n")[0].split(",")
  if vals[0] not in fWeights:
    fWeights[vals[0]]=dict()
  fWeights[vals[0]][vals[1]]=float(vals[2])



#Beginning of the actual algorithm
total=list()
for run in xrange(1, 3):
  arrivals=dict()
  f_arr=open(ROOT + "GraphStructure/Arrivals/" + str(run), "r")

  f_LHS=open(ROOT + "reduced_movies.dat", "r")

  av_LHS=dict()
  for line in f_LHS:
    vals=line.split("\n")[0]
    av_LHS[int(vals.split("::")[0])]=0
  f_LHS.close()  

  arr=dict()
  t=0
  for line in f_arr:
    vals=line.split("\n")[0]
    arr[t]=vals
    t+=1

  ALG=0
  chosenEdges=set()
  for t in xrange(T):
    arr_v=arr[t]
    
    if arr_v == "-1":
      continue
    LHS_v=neigh_RHS[arr_v]
   
    for _eta in xrange(eta):
      #Making the Greedy choice
      maximum=-1
      maxEdge=""
      for _lhs in LHS_v:
        if av_LHS[int(_lhs)]>=b:
          continue
        edge=reverse_edge[arr_v + "<>" + _lhs]
        
        _chosenEdges=chosenEdges
        _value=computeF(_chosenEdges.union(edge))-computeF(chosenEdges)
        
        
        if _value>maximum:
          maximum=_value
          maxEdge=edge
      
      if maximum == -1:
        continue
      chosenEdges.add(maxEdge)
      av_LHS[int(edges[maxEdge]["LHS"])]+=1
  
  ALG=computeF(chosenEdges)
  total.append(ALG/OPT) 

total_means=list()
cnt=0
_sum=0
for _tot in total:
  _sum+=_tot
  if cnt%2 == 1:
    total_means.append(_sum/2.0)
    _sum=0
  cnt+=1

print mean(total_means)
  



   



   









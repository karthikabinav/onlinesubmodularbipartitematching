import sys
import numpy as np
import cvxopt
from cvxopt import matrix, spmatrix, solvers


T=int(sys.argv[1])
exp=sys.argv[2]
bM=int(sys.argv[3])
num_cov=1

ROOT="./datasets/IBM_HR_dataset/GraphStructure/"

f_LHS=open(ROOT + "LHS_types_sim", "r")
LHS=dict()
U=0
av_LHS=dict()
for line in f_LHS:
  vals=line.split("\n")[0]
  LHS[vals]=U
  av_LHS[vals]=True
  U+=1
f_LHS.close()  

print "U", U

f_RHS=open(ROOT + "RHS_types_sim", "r")
RHS=dict()
V=0

for line in f_RHS:
  vals=line.split("\n")[0]
  RHS[vals]=V
  V+=1

print "V", V
f_RHS.close()

f_rates=open(ROOT + "arrival_rates", "r")
rates=dict()

for line in f_rates:
  vals=line.split("\n")[0].split(",")
  rates[vals[0]]=float(vals[1])

f_rates.close()

f_edges=open(ROOT + "edges_sim", "r")

edges=dict()
neigh_RHS=dict()
neigh_LHS=dict()
reverse_edge=dict()
E=0

all_features=set()
for line in f_edges:
  vals=line.split("\n")[0].split(",")
  E+=1
  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["weight"]=vals[3]
  edges[vals[0]]["features"]=set()
  
  reverse_edge[vals[1] + "<>" + vals[2]]=vals[0]
  if vals[1] not in neigh_RHS:
    neigh_RHS[vals[1]]=set()
  
  if vals[2] not in neigh_LHS:
    neigh_LHS[vals[2]]=set()

  neigh_RHS[vals[1]].add(vals[2])
  neigh_LHS[vals[2]].add(vals[1])

  feat1=set(vals[1].split(";"))
  feat2=set(vals[2].split(";"))
  feat=feat1.union(feat2)
  
  for _f in feat:
    edges[vals[0]]["features"].add(_f)
    all_features.add(_f)

f_edges.close()

# A and b matrix
K=0
resource_map=dict()

for _all in all_features:
  resource_map[_all]=K
  K+=1

fWeights=dict()
f_fweights=open(ROOT + "featureWeights", "r")
for line in f_fweights:
  vals=line.split("\n")[0].split(",")
  fWeights[vals[0]]=float(vals[1])

#Weights
W=[0 for i in xrange(E)]
W=W+[fWeights[str(i+1)] for i in xrange(K)]


b=[0 for i in xrange(K+U+V)]
valsl=list()
I=list()
J=list()

#+1 in indices to account for matlab 1 index
for _e in xrange(E):
  for _feat in edges[str(_e)]["features"]:
    valsl.append(-1)
    k=resource_map[_feat]
    I.append(k + 1)
    J.append(_e + 1)

for k in xrange(K):
  valsl.append(1)
  I.append(k + 1)
  J.append(E + k + 1)

for _v in RHS:
  b[RHS[_v]+K]=rates[_v]
  for _neigh_RHS in neigh_RHS[_v]:
    _edge=_v + "<>" + _neigh_RHS
    _edgeNum=int(reverse_edge[_edge])
    
    valsl.append(1)
    I.append(K + RHS[_v] + 1)
    J.append(_edgeNum + 1)

for _u in LHS:
  b[K+V+LHS[_u]]=bM
  for _neigh_LHS in neigh_LHS[_u]:
    _edge=_neigh_LHS + "<>" + _u
    _edgeNum=int(reverse_edge[_edge])
    
    valsl.append(1)
    I.append(K + V + LHS[_u] + 1)
    J.append(_edgeNum + 1)

f = open("./LP/vals"+exp, "w+")
np.array(valsl).tofile(f, sep=",")
f.close()
f = open("./LP/I"+exp, "w+")
np.array(I).tofile(f, sep=",")
f.close()
f = open("./LP/J"+exp, "w+")
np.array(J).tofile(f, sep=",")
f.close()
f = open("./LP/b"+exp, "w+")
print "b=", len(b)
b=matrix(b)
np.array(b).tofile(f, sep=",")
f.close()
f = open("./LP/c"+exp, "w+")
print "W=", len(W)
W=matrix(W)
np.array(W).tofile(f, sep=",")
f.close()

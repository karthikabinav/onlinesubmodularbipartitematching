import random
import sys 

T=int(sys.argv[1])


for i in xrange(1, 21):
  run=str(i)

  ROOT="GraphStructure/"

  f_RHS=open(ROOT + "arrival_rates", "r")
  RHS=dict()
  for line in f_RHS:
    vals=line.split("\n")[0].split(",")
    RHS[vals[0]]=float(vals[1])/T
  f_RHS.close()

  g=open("GraphStructure/Arrivals/" + run, "w+")
  for t in xrange(T):
    r=random.uniform(0, 1)
    rsum=0
    wr=False
    for _rhs in RHS:
      if rsum + RHS[_rhs]>=r and rsum<r:
        g.write(_rhs + "\n")
        wr=True
        break
      rsum+=RHS[_rhs]
      assert(rsum<=1)
    if not wr:
      g.write("-1" + "\n")

  g.close()





import sys
import random


ROOT="./GraphStructure/"

f_RHS=open(ROOT + "RHS_types_sim", "r")

g=open(ROOT + "arrival_rates", "w+")
rates=dict()
total=0
for line in f_RHS:
  vals=line.split("\n")[0]
  r=random.uniform(0.5, 0.7)
  #r=1
  
  rates[vals]=r
  total+=r

for _r in rates:
  g.write(_r +"," + str(rates[_r]) + "\n")
g.close()


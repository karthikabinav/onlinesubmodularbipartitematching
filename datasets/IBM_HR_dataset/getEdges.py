import random

f=open("GraphStructure/RHS_types_sim", "r")

RHS=dict()

Rcnt=0
for line in f:
  vals=line.split("\n")[0]
  RHS[Rcnt]=vals
  Rcnt+=1

f.close()

f=open("GraphStructure/LHS_types_sim", "r")

LHS=dict()

Lcnt=0
for line in f:
  vals=line.split("\n")[0]
  LHS[Lcnt]=vals
  Lcnt+=1

E=0
f=open("GraphStructure/edges_sim", "w+")
for r in RHS:
  num=random.randint(5, 15)
  _neigh=random.sample(range(0, Lcnt), num)
  
  for neigh in _neigh:
    weight=random.uniform(0, 1)
    wr=str(E) + "," + str(RHS[r]) + "," + LHS[neigh] + "," + str(weight) + "\n"
    f.write(wr)
    E+=1
f.close()





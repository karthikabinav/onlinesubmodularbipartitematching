import sys
import random

n=int(sys.argv[1])

f=open("GraphStructure/LHS_types_sim", "w+")
for i in xrange(n):
  num=random.randint(5,10)
  s=random.sample(range(1, 1000), num)
  
  w=""
  for _s in s:
    w=w+str(_s) + ";"
  w=w[:-1]
  f.write(w + "\n")
f.close() 


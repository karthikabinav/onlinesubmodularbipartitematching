import sys
import numpy as np
import cvxopt
from cvxopt import matrix, spmatrix, solvers

T=int(sys.argv[1])
exp=sys.argv[2]
bM=int(sys.argv[3])
B=int(sys.argv[4])
num_cov=1

ROOT="./datasets/IBM_HR_dataset/GraphStructure/"

f_LHS=open(ROOT + "LHS_types_sim", "r")
LHS=dict()
U=0
for line in f_LHS:
  vals=line.split("\n")[0]
  LHS[vals]=U
  U+=1
f_LHS.close()  

print "U", U
'''
f_RHS=open(ROOT + "RHS_types_sim", "r")
RHS=dict()
V=0

for line in f_RHS:
  vals=line.split("\n")[0]
  RHS[vals]=V
  V+=1

print "V", V
f_RHS.close()
'''
f_rates=open(ROOT + "arrival_rates", "r")
RHS=dict()
V=0
for line in f_rates:
  vals=line.split("\n")[0].split(",")
  if vals[0] not in RHS:
    RHS[vals[0]]=dict()
  RHS[vals[0]]["id"]=V
  RHS[vals[0]]["rate"]=float(vals[1])
  V+=1

f_rates.close()
print "V", V

f_edges=open(ROOT + "edges_sim", "r")

edges=dict()
neigh_RHS=dict()
neigh_LHS=dict()
reverse_edge=dict()
E=0

for line in f_edges:
  vals=line.split("\n")[0].split(",")
  assert(int(vals[0]) == E)
  E+=1
  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["weight"]=float(vals[3])
  
  reverse_edge[vals[1] + "<>" + vals[2]]=vals[0]
  if vals[1] not in neigh_RHS:
    neigh_RHS[vals[1]]=set()
  
  if vals[2] not in neigh_LHS:
    neigh_LHS[vals[2]]=set()

  neigh_RHS[vals[1]].add(vals[2])
  neigh_LHS[vals[2]].add(vals[1])
f_edges.close()


print "E=", E
#Weights
W=[0 for i in xrange(E)]
W=W+[B]

b=list()
valsl=list()
I=list()
J=list()

#+1 in indices to account for matlab 1 index
for _e in xrange(E):
  weight=edges[str(_e)]["weight"]
  valsl.append(-weight)
  I.append(1)
  J.append(_e + 1)

b=[0 for i in xrange(1+U+V)]

valsl.append(B)
I.append(1)
J.append(E + 1)
for _v in RHS:
  b[RHS[_v]["id"]+1] = RHS[_v]["rate"]
  for _neigh_RHS in neigh_RHS[_v]:
    _edge=_v + "<>" + _neigh_RHS
    _edgeNum=int(reverse_edge[_edge])
    
    valsl.append(1)
    I.append(1 + RHS[_v]["id"] + 1)
    J.append(_edgeNum + 1)
for _u in LHS:
  b[1+V+LHS[_u]] = bM
  for _neigh_LHS in neigh_LHS[_u]:
    _edge=_neigh_LHS + "<>" + _u
    _edgeNum=int(reverse_edge[_edge])
    
    valsl.append(1)
    I.append(1 + V + LHS[_u] + 1)
    J.append(_edgeNum + 1)

f = open("./LP/vals"+exp, "w+")
np.array(valsl).tofile(f, sep=",")
f.close()
f = open("./LP/I"+exp, "w+")
np.array(I).tofile(f, sep=",")
f.close()
f = open("./LP/J"+exp, "w+")
np.array(J).tofile(f, sep=",")
f.close()
f = open("./LP/b"+exp, "w+")
print "b=", len(b)
b=matrix(b)
np.array(b).tofile(f, sep=",")
f.close()
f = open("./LP/c"+exp, "w+")
print "W=", len(W)
W=matrix(W)
np.array(W).tofile(f, sep=",")
f.close()

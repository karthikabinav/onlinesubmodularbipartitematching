import sys
from statistics import median
import random

#Computing the coverage function
def computeF(S):
  covered=set()
  for e in S:
    covered.add(e)
  
  total=0
  for e in covered:
    ed_weight=edges[e]["weight"]
    total = total + ed_weight
  return min(B, total)

T=int(sys.argv[1])
OPT=float(sys.argv[2])
b=int(sys.argv[3])
B=int(sys.argv[4])
num_cov=1


ROOT="../../datasets/IBM_HR_dataset/GraphStructure/"

fWeights=dict()
f_fweights=open(ROOT + "featureWeights", "r")
for line in f_fweights:
  vals=line.split("\n")[0].split(",")
  fWeights[vals[0]]=float(vals[1])

f_LHS=open(ROOT + "LHS_types_sim", "r")
LHS=dict()
U=0
av_LHS=dict()
for line in f_LHS:
  vals=line.split("\n")[0]
  LHS[vals]=U
  av_LHS[vals]=0
  U+=1
f_LHS.close()  

f_RHS=open(ROOT + "RHS_types_sim", "r")
RHS=dict()
V=0

for line in f_RHS:
  vals=line.split("\n")[0]
  RHS[vals]=V
  V+=1
 
f_RHS.close()

f_rates=open(ROOT + "arrival_rates", "r")
rates=dict()

for line in f_rates:
  vals=line.split("\n")[0].split(",")
  rates[vals[0]]=float(vals[1])



f_edges=open(ROOT + "edges_sim", "r")

edges=dict()
neigh_RHS=dict()
neigh_LHS=dict()
reverse_edge=dict()
E=0
for line in f_edges:
  vals=line.split("\n")[0].split(",")

  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["weight"]=float(vals[3])
  edges[vals[0]]["features"]=set()
  E+=1
  reverse_edge[vals[1] + "<>" + vals[2]]=vals[0]
  if vals[1] not in neigh_RHS:
    neigh_RHS[vals[1]]=set()
  if vals[2] not in neigh_LHS:
    neigh_LHS[vals[2]]=set()

  neigh_RHS[vals[1]].add(vals[2])
  neigh_LHS[vals[2]].add(vals[1])
  
  feat1=set(vals[1].split(";"))
  feat2=set(vals[2].split(";"))

  feat=feat1.union(feat2)
  
  for _f in feat:
    edges[vals[0]]["features"].add(_f)
f_edges.close()

f_X=open("../../LP/Xbudget" + str(b), "r")
X=dict()
EE=0
for line in f_X:
  vals=line.split("\n")[0]
  X[str(EE)]=float(vals)/rates[edges[str(EE)]["RHS"]]

  EE+=1

  if EE>=E:
    break

f_X.close()


av_edges=dict()
#Negative CR sampling on LHS
for _lhs in LHS:
  r=random.uniform(0, 1)
  rsum=0
  for _neigh in neigh_LHS[_lhs]:
    edge=_neigh + "<>" + _lhs
    edgeNum=reverse_edge[edge]
  
    if rsum + X[edgeNum]>=r and rsum<r:
      av_edges[edgeNum]=True
    rsum+=X[edgeNum]

#Beginning of the actual algorithm
total=list()
for run in xrange(1, 21):
  arrivals=dict()
  f_arr=open(ROOT + "Arrivals/" + str(run), "r")
  
  f_LHS=open(ROOT + "LHS_types_sim", "r")
  av_LHS=dict()
  for line in f_LHS:
    vals=line.split("\n")[0]
    av_LHS[vals]=0
  f_LHS.close()  

  arr=dict()
  t=0
  for line in f_arr:
    vals=line.split("\n")[0]
    arr[t]=vals
    t+=1

  ALG=0
  chosenEdges=set()
  for t in xrange(T):
    arr_v=arr[t]
    
    if arr_v == "-1":
      continue

    LHS_v=neigh_RHS[arr_v]
    
    #Making the Greedy choice
    maximum=-1
    maxEdge=""

        
    tot_av_LHS=0
    for _lhs in LHS_v:
      edge=reverse_edge[arr_v + "<>" + _lhs]
      if av_LHS[_lhs]<b and edge in av_edges and av_edges[edge]:
        tot_av_LHS+=1
    if tot_av_LHS==0:
      continue

    r=random.uniform(0, 1)
    rsum=0
    for _lhs in LHS_v:
      edge=reverse_edge[arr_v + "<>" + _lhs]
      if not (av_LHS[_lhs]<b and edge in av_edges and av_edges[edge]):
        continue
      if rsum+1.0/tot_av_LHS>=r and rsum<r:
          maxEdge=edge
          maximum=0
          break
      rsum+=1.0/tot_av_LHS
    assert(maximum!=-1)
    if maximum == -1:
      continue
    chosenEdges.add(maxEdge)
    av_LHS[edges[maxEdge]["LHS"]]+=1
  
  ALG=computeF(chosenEdges)
  total.append(ALG/OPT) 

total_means=list()
cnt=0
_sum=0
for _tot in total:
  _sum+=_tot
  if cnt%2 == 1:
    total_means.append(_sum/2.0)
    _sum=0
  cnt+=1

print median(total_means)

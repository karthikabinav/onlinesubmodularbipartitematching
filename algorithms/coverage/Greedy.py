import sys
from statistics import median

#Computing the coverage function
def computeF(S):
  coveredE=set()
  covered=set()
  for e in S:
    coveredE.add(e)
  
  for e in coveredE:
    ed_features=edges[e]["features"]

    for _features in ed_features:
        covered.add(_features)
  total=0
  for _cov in covered:
    total = total + fWeights[_cov]
  return total

T=int(sys.argv[1])
OPT=float(sys.argv[2])
b=int(sys.argv[3])
num_cov=1


ROOT="../../datasets/IBM_HR_dataset/GraphStructure/"

fWeights=dict()
f_fweights=open(ROOT + "featureWeights", "r")
for line in f_fweights:
  vals=line.split("\n")[0].split(",")
  fWeights[vals[0]]=float(vals[1])

f_LHS=open(ROOT + "LHS_types_sim", "r")
LHS=dict()
U=0
av_LHS=dict()
for line in f_LHS:
  vals=line.split("\n")[0]
  LHS[vals]=U
  av_LHS[vals]=0
  U+=1
f_LHS.close()  

f_RHS=open(ROOT + "RHS_types_sim", "r")
RHS=dict()
V=0

for line in f_RHS:
  vals=line.split("\n")[0]
  RHS[vals]=V
  V+=1
 
f_RHS.close()

f_edges=open(ROOT + "edges_sim", "r")

edges=dict()
neigh=dict()
reverse_edge=dict()

for line in f_edges:
  vals=line.split("\n")[0].split(",")

  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["weight"]=vals[3]
  edges[vals[0]]["features"]=set()
  
  reverse_edge[vals[1] + "<>" + vals[2]]=vals[0]
  if vals[1] not in neigh:
    neigh[vals[1]]=set()
  
  neigh[vals[1]].add(vals[2])

  feat1=set(vals[1].split(";"))
  feat2=set(vals[2].split(";"))

  feat=feat1.union(feat2)
  #print len(feat) 
  for _f in feat:
    edges[vals[0]]["features"].add(_f)
f_edges.close()
  
#Beginning of the actual algorithm
total=list()
for run in xrange(1, 21):
  arrivals=dict()
  f_arr=open(ROOT + "Arrivals/" + str(run), "r")
  
  f_LHS=open(ROOT + "LHS_types_sim", "r")
  av_LHS=dict()
  for line in f_LHS:
    vals=line.split("\n")[0]
    av_LHS[vals]=0
  f_LHS.close()  

  arr=dict()
  t=0
  for line in f_arr:
    vals=line.split("\n")[0]
    arr[t]=vals
    t+=1

  ALG=0
  chosenEdges=set()
  for t in xrange(T):
    arr_v=arr[t]
    
    if arr_v == "-1":
      continue

    LHS_v=neigh[arr_v]
    
    #Making the Greedy choice
    maximum=-1
    maxEdge=""
    for _lhs in LHS_v:
      if av_LHS[_lhs]>=b:
        continue
      edge=reverse_edge[arr_v + "<>" + _lhs]
      
      _chosenEdges=chosenEdges
      _value=computeF(_chosenEdges.union(edge))-computeF(chosenEdges)
      
      
      if _value>maximum:
        maximum=_value
        maxEdge=edge
    
    if maximum == -1:
      continue
    chosenEdges.add(maxEdge)
    #print len(chosenEdges)
    av_LHS[edges[maxEdge]["LHS"]]+=1
  
  ALG=computeF(chosenEdges)
  total.append(ALG/OPT) 

total_means=list()
cnt=0
_sum=0
for _tot in total:
  _sum+=_tot
  if cnt%2 == 1:
    total_means.append(_sum/2.0)
    _sum=0
  cnt+=1

print median(total_means)
  



   



   









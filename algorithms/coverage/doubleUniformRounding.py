import sys
from statistics import median
import random

#Computing the coverage function
def computeF(S):
  coveredE=set()
  covered=set()
  for e in S:
    coveredE.add(e)
  
  for e in coveredE:
    ed_features=edges[e]["features"]

    for _features in ed_features:
        covered.add(_features)
  total=0
  for _cov in covered:
    total = total + fWeights[_cov]
  return total

T=int(sys.argv[1])
OPT=float(sys.argv[2])
b=int(sys.argv[3])
num_cov=1


ROOT="../../datasets/IBM_HR_dataset/GraphStructure/"

fWeights=dict()
f_fweights=open(ROOT + "featureWeights", "r")
for line in f_fweights:
  vals=line.split("\n")[0].split(",")
  fWeights[vals[0]]=float(vals[1])


f_LHS=open(ROOT + "LHS_types_sim", "r")
LHS=dict()
U=0
av_LHS=dict()
for line in f_LHS:
  vals=line.split("\n")[0]
  LHS[vals]=U
  av_LHS[vals]=0
  U+=1
f_LHS.close()  

f_RHS=open(ROOT + "RHS_types_sim", "r")
RHS=dict()
V=0

for line in f_RHS:
  vals=line.split("\n")[0]
  RHS[vals]=V
  V+=1
 
f_RHS.close()

f_rates=open(ROOT + "arrival_rates", "r")
rates=dict()

for line in f_rates:
  vals=line.split("\n")[0].split(",")
  rates[vals[0]]=float(vals[1])



f_edges=open(ROOT + "edges_sim", "r")
E=0
edges=dict()
neigh_RHS=dict()
neigh_LHS=dict()
reverse_edge=dict()

for line in f_edges:
  vals=line.split("\n")[0].split(",")
  E+=1
  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["weight"]=vals[3]
  edges[vals[0]]["features"]=set()
  
  reverse_edge[vals[1] + "<>" + vals[2]]=vals[0]
  if vals[1] not in neigh_RHS:
    neigh_RHS[vals[1]]=set()
  if vals[2] not in neigh_LHS:
    neigh_LHS[vals[2]]=set()

  neigh_RHS[vals[1]].add(vals[2])
  neigh_LHS[vals[2]].add(vals[1])

  feat=vals[1].split(";")
  feat=feat+vals[2].split(";")
  
  for _f in feat:
    edges[vals[0]]["features"].add(_f)
f_edges.close()

f_X=open("../../LP/Xcoverage" + str(b), "r")
X=dict()
EE=0
for line in f_X:
  vals=line.split("\n")[0]
  X[str(EE)]=float(vals)/rates[edges[str(EE)]["RHS"]]
  EE+=1

  if EE>=E:
    break

f_X.close()


#Independent Sampling
av_edges=dict()
cnt=0
for _rev_ed in reverse_edge:
  _ed=reverse_edge[_rev_ed]
  _r=random.uniform(0, 1)
  if _r<=X[_ed]:
    av_edges[_ed]=True
    cnt+=1
#Independent sampling on LHS
for _lhs in LHS:
  #count number of edges incident to _lhs
  cnt=0
  for _neigh in neigh_LHS[_lhs]:
    edge=_neigh + "<>" + _lhs
    if reverse_edge[edge] in av_edges:
      cnt+=1
  
  r=random.uniform(0, 1)
  rsum=0

  for _neigh in neigh_LHS[_lhs]:
    edge=_neigh + "<>" + _lhs
    if reverse_edge[edge] not in av_edges:
      continue

    if rsum + 1.0/cnt>=r and rsum<r:
      av_edges[edge]=True
    else:
      av_edges[edge]=False
    rsum+=1.0/cnt

#Beginning of the actual algorithm
total=list()
for run in xrange(1, 21):
  arrivals=dict()
  f_arr=open(ROOT + "Arrivals/" + str(run), "r")
  
  f_LHS=open(ROOT + "LHS_types_sim", "r")
  av_LHS=dict()
  for line in f_LHS:
    vals=line.split("\n")[0]
    av_LHS[vals]=0
  f_LHS.close()  

  arr=dict()
  t=0
  for line in f_arr:
    vals=line.split("\n")[0]
    arr[t]=vals
    t+=1

  ALG=0
  chosenEdges=set()
  for t in xrange(T):
    arr_v=arr[t]
    
    if arr_v == "-1":
      continue

    LHS_v=neigh_RHS[arr_v]
    
    #Making the Greedy choice
    maximum=-1
    maxEdge=""

        
    tot_av_LHS=0
    for _lhs in LHS_v:
      edge=reverse_edge[arr_v + "<>" + _lhs]
      if av_LHS[_lhs]<b and edge in av_edges and av_edges[edge]:
        tot_av_LHS+=1
    if tot_av_LHS==0:
      continue

    r=random.uniform(0, 1)
    rsum=0
    for _lhs in LHS_v:
      edge=reverse_edge[arr_v + "<>" + _lhs]
      if not (av_LHS[_lhs]<b and edge in av_edges and av_edges[edge]):
        continue
      if rsum+1.0/tot_av_LHS>=r and rsum<r:
          maxEdge=edge
          maximum=0
          break
      rsum+=1.0/tot_av_LHS
    assert(maximum!=-1)
    if maximum == -1:
      continue
    chosenEdges.add(maxEdge)
    av_LHS[edges[maxEdge]["LHS"]]+=1
  
  ALG=computeF(chosenEdges)
  total.append(ALG/OPT) 

total_means=list()
cnt=0
_sum=0
for _tot in total:
  _sum+=_tot
  if cnt%2 == 1:
    total_means.append(_sum/2.0)
    _sum=0
  cnt+=1

print median(total_means)
